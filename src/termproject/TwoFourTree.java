package termproject;

import java.util.Random;

/**
 * Title:       Term Project 2-4 Trees
 * Description: Implements design for 2-4 Tree based on Dictionary interface
 * Copyright:   Copyright (c) 2001
 * Company:     Cedarville University
 * @authors     Ben Seymour, Blake Gambrell
 * @version 1.0
 * 
 * Summary of modifications:
 *  12/11 1:00, BDS: Added check for empty tree in findElement, insertElement,
 *      and removeElement. Added whitespace and more comments to increase 
 *      readability.
 *  The rest of this project's modification history can be found at
 *      https://bitbucket.org/BDSeymour/termproject/commits/all 
 *      - the repository used during development should now be publicly 
 *      viewable
 */
public class TwoFourTree
        implements Dictionary {

    private Comparator treeComp;
    private int size;
    private TFNode treeRoot;
    private final int OUT_OF_RANGE;

    public TwoFourTree(Comparator comp) {
        this.treeRoot = null;
        this.OUT_OF_RANGE = 10;
        this.size = 0;
        treeComp = comp;
    }

    private TFNode root() {
        return treeRoot;
    }

    private void setRoot(TFNode root) {
        treeRoot = root;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return (size == 0);
    }

    /**
     * Searches dictionary to determine if key is present
     * @param key to be searched for
     * @return object corresponding to key; null if not found
     */
    @Override
    public Object findElement(Object key) throws ElementNotFoundException {
        if(isEmpty()){
            throw new TwoFourTreeException("Tree is empty");
        }

        //Locate the node the key should be present in
        TFNode a = find(treeRoot, key);
        
        //Search the TFNode for the key
        for(int i = 0; i <= a.getNumItems()-1; i++){
            if(treeComp.isEqual(a.getItem(i).key(), key)){
                return a.getItem(i).element();
            }
        }
        //If key is not found, throw exception
        throw new ElementNotFoundException("No element with specified key");
    }
    
    /**
     * Searches the dictionary to return node where key should be if present 
     *  in tree.
     * @param a
     * @param key
     * @return TFNode that the element searching for is in
     */
    private TFNode find(TFNode a, Object key){
        //test to see if the nodei s external
       if(a.isExternal()){
           return a;
       }
        //loops through the items present in a
        for(int i = 0; i <= a.getNumItems()-1; i++){
           Item arrayItem = a.getItem(i);
           //return this node if equal
           if(treeComp.isEqual(arrayItem.key(), key)){
               return a;
           }
           else if(treeComp.isGreaterThan(arrayItem.key(), key)){
               //otherwise step to the next child
               return find(a.getChild(i), key);
           }
        }
        //if key > all in a then step to right most child
        return find(a.getChild(a.getNumItems()), key);
    }

    /**
     * Inserts provided element into the Dictionary
     * @param key of object to be inserted
     * @param element to be inserted
     */
    @Override
    public void insertElement(Object key, Object element) {
        Item newElement = new Item(key, element);
        
        //Empty tree case
        if(isEmpty()){
            setRoot(new TFNode());
            root().addItem(0, newElement);
            size++;
            return;
        }
        
        TFNode insertPoint = find(treeRoot, key);
        
        //Checks if the insertPoint is not external - this only happens if 
        //another copy of the key to be inserted already exists in the tree.
        //If node is not external, walks to leaf node with inorder successor
        //  -  Changed to not external (B.G.)
        if(!(insertPoint.isExternal())){
            //Looks for key equal to key to be inserted
            for(int i = 0; i <= insertPoint.getNumItems()-1; i++){
                if(treeComp.isEqual(key, insertPoint.getItem(i).key())){
                     insertPoint = insertPoint.getChild(i+1);
                     break;
                 }
            }
            //then walk left till insertPoint is at a leaf
            while( !( insertPoint.isExternal() ) ){
                insertPoint = insertPoint.getChild(0);
            }
            //Inserting where the inorder successor is, so insert is simple
            insertPoint.insertItem(0,newElement);
        }
        
        //After confirming current node is external, find place in node to 
        //insert Item, and perform insert. 
        else{
            int i;
            for(i = 0; i < insertPoint.getNumItems(); i++){
               if(treeComp.isLessThanOrEqualTo(key, 
                       insertPoint.getItem(i).key())){
                   
                   break;
               }
            }
            insertPoint.insertItem(i, newElement);
        }

        if(insertPoint.getNumItems()==4){
            fixInsert(insertPoint);
            size++;
        }
    }
    
    //method to fix the nodes
    private void fixInsert(TFNode fixNode){
        
        //Creating temporary nodes and items to move ones that are shifted 
        //around during deletes and inserts.
        TFNode tempNode1 = fixNode.getChild(2);
        TFNode tempNode2 = fixNode.getChild(3);
        TFNode tempNode3 = fixNode.getChild(4);
        
        Item tempItem1 = fixNode.deleteItem(3);
        Item tempItem2 = fixNode.deleteItem(2);
        
        //Root case (root has 4 Items)
        if(fixNode == root()){
            //Create new root node
            TFNode newRoot = new TFNode();
            newRoot.addItem(0, tempItem2);
            TFNode newChild = new TFNode();
            newChild.addItem(0, tempItem1);
            
            newRoot.setChild(0, fixNode);
            newRoot.setChild(1, newChild);
            
            setRoot(newRoot);
            fixNode.setParent(root());
            newChild.setParent(root());
            
            fixNode.setChild(2, tempNode1);
            newChild.setChild(0, tempNode2);
            newChild.setChild(1, tempNode3);
            
            if( !(fixNode.isExternal()) ){
                tempNode1.setParent(fixNode);
                tempNode2.setParent(newChild);
                tempNode3.setParent(newChild);
            }
        }
        
        //Standard case
        else{
            TFNode parentNode = fixNode.getParent();

           //turn tempItem1 into a new node
           TFNode insertNode = new TFNode();
           insertNode.addItem(0, tempItem1); 

           //Begin Dr. G's code
           int wcai = fixNode.whatChildIsThis();
           parentNode.insertItem(wcai, tempItem2);
           
           parentNode.setChild(wcai+1, insertNode);
           insertNode.setParent(parentNode);
           
        
           if (tempNode2 != null){
               insertNode.setChild(0, tempNode2);
               tempNode2.setParent(insertNode);
               insertNode.setChild(1, tempNode3);
               tempNode3.setParent(insertNode);
           }
           //End Dr. G's code
        
           //Standard case may cause parent overflow. In this case, the 
           //parent's children need to be kept track of so they can be put 
           //back in the correct places.
           if(parentNode.getNumItems()==4){
               fixInsert(parentNode);
           }  
       }
       size++;
   }
    

    /**
     * Searches dictionary to determine if key is present, then
     * removes and returns corresponding object
     * @param key of data to be removed
     * @return object corresponding to key
     * @exception ElementNotFoundException if the key is not in dictionary
     */
    @Override
    public Object removeElement(Object key) throws ElementNotFoundException {
        //Empty tree case
        if(isEmpty()){
            throw new TwoFourTreeException("Tree is empty");
        }
        
        //Get node where key should be if present in tree
        TFNode removePoint = find(root(), key);
        
        //Check if key is present in node
        //If so, store index of Item that contains the key
        int removeIndex = OUT_OF_RANGE;
        for(int i = 0; i <= removePoint.getNumItems()-1; i++){
           if(treeComp.isEqual(key, removePoint.getItem(i).key())){
               removeIndex = i;
               break;
           }
        }
        
        //If node did not contain key, throw exception
        if(removeIndex == OUT_OF_RANGE){
            throw new ElementNotFoundException
                    ("No element with specified key");
        }

    //If node did contain key:  
        //If node is external and has > 1 items, just remove item, 
        //and removeElement is done
        if(removePoint.isExternal() && removePoint.getNumItems() > 1){
            return removePoint.removeItem(removeIndex).element();
        }
        
        else if(removePoint.isExternal()){
            Item removedItem = removePoint.removeItem(removeIndex);
            if(removePoint == root()){
                size--;
                return removedItem.element();
            }
            fixUnderflow(removePoint);
            return removedItem.element();
        }
        
        else{
            //Substitute Item to be removed with its inorder predecessor 
            //(at leaf), remove the item at the leaf. Clean up any underflow.
            
            //First, find substitute (inorder successor) by key
            TFNode swapNode = removePoint.getChild(removeIndex + 1);
            while( !( swapNode.isExternal() ) ){
                swapNode = swapNode.getChild(0);
            }
            //Remove original copy of substitute from leaf, storing to swap 
            //in at removePoint
            Item swapItem = swapNode.removeItem(0);
            
            //Get Item called for by replacing it with swapItem (this deletes 
            //the Item called for from the tree)
            Item removedItem = removePoint.replaceItem(removeIndex, swapItem);
            
            //If removal did not cause underflow, return element of 
            //removedItem
            if(swapNode.getNumItems() > 0)
                return removedItem.element();
            //If removal caused underflow, fixUnderflow, then return element 
            //of removedItem
            else{
                fixUnderflow(swapNode);
                return removedItem.element();
            }
        }
    }
    
    private void fixUnderflow(TFNode fixNode){
        //Check if fixNode has a left sibling
        
        if (fixNode == treeRoot) {
            treeRoot = treeRoot.getChild(0);
        }
        if(fixNode.whatChildIsThis() > 0){
            TFNode leftSibling = fixNode.getParent().getChild
                    (fixNode.whatChildIsThis() - 1);
            
            //If left sibling has enough items, do left transfer
            if(leftSibling.getNumItems() > 1){
                //Store node moving down from parent as downShifter, emptying 
                //its spot in parent
                Item downShifter = fixNode.getParent().deleteItem
                        (fixNode.whatChildIsThis() - 1);
                //Store child coming over from leftSibling as transferKid
                TFNode transferKid = leftSibling.getChild
                        (leftSibling.getNumItems());
                
                //Place downShifter in empty spot in fixNode (insertItem 
                //copies fixNode's current child to index 1)
                fixNode.insertItem(0, downShifter);
                //Place transferKid at child index 0 of fixNode
                fixNode.setChild(0, transferKid);
                if (transferKid != null) {
                    transferKid.setParent(fixNode);
                }
                //Move rightmost Item from leftSibling into empty spot in 
                //parent
                fixNode.getParent().addItem(leftSibling.whatChildIsThis(), 
                        leftSibling.deleteItem(leftSibling.getNumItems()-1));
                
                //Cleanup: null transferKid's original location
                leftSibling.setChild(leftSibling.getNumItems()+2, null);
            }
            
            //If left transfer isn't possible, call fuseLeft instead
            else fuseLeft(fixNode);
        }
        
        else{
            //If this node is child 0 of its parent, attempt transfer from 
            //right instead. rightSibling should be child 1
            TFNode rightSibling = fixNode.getParent().getChild(1);
            if(rightSibling.getNumItems() > 1){
                //Store node moving down from parent as downShifter, emptying 
                //its spot in parent
                Item downShifter = fixNode.getParent().deleteItem(0);
                //Store child coming over from leftSibling as transferKid
                TFNode transferKid = rightSibling.getChild(0);
                
                //Place downShifter in empty spot in fixNode
                fixNode.insertItem(fixNode.getNumItems(), downShifter);
                //Set transferKid as rightmost child of fixNode
                fixNode.setChild(fixNode.getNumItems(), transferKid);
                if (transferKid != null) {
                    transferKid.setParent(fixNode);
                }
                //Move leftmost Item from rightSibling into empty spot in 
                //parent, removing it and leftmost child from rightSibling
                fixNode.getParent().addItem(0, rightSibling.removeItem(0));
            }
            
            //If right transfer isn't possible, call fuseRight instead
            else fuseRight(fixNode);
        }
    }
          
    public void fuseLeft(TFNode emptyFuse){
        //Naming our empty node's parent, its left sibling, and leftSibling's 
        //children
        TFNode parent = emptyFuse.getParent();
        TFNode leftSibling = parent.getChild(emptyFuse.whatChildIsThis() - 1);
        TFNode lChild0 = leftSibling.getChild(0);
        TFNode lChild1 = leftSibling.getChild(1);
        
        //Slide element from leftSibling into emptyFuse
        emptyFuse.insertItem(0, leftSibling.removeItem(0));
        //Slide leftmost Item from parent down to node to fuse with.
        emptyFuse.insertItem(1, parent.removeItem
                (emptyFuse.whatChildIsThis() - 1) );
        //emptyFuse is no longer empty, and its original child should be 
        //duplicated in all three child indices. Set leftSibling's children in 
        //indices 0 and 1
        emptyFuse.setChild(0, lChild0);
        emptyFuse.setChild(1, lChild1);
        
        //emptyFuse is no longer empty, and its original child should be 
        //duplicated in all three child indices. Set leftSibling's children in 
        //indices 0 and 1 (not necessary if emptyFuse is external)
        if( !(emptyFuse.isExternal()) ){
                emptyFuse.setChild(0, lChild0);
                lChild0.setParent(emptyFuse);
                emptyFuse.setChild(1, lChild1);
                lChild1.setParent(emptyFuse);
        }
        
        if(parent == root() && root().getNumItems() == 0){
            setRoot(emptyFuse);
            emptyFuse.setParent(null);
        }
        else {
            if(parent.getNumItems() == 0){
                fixUnderflow(parent);
                size--;
        }
        }
        size--;
    }
    
    /**
     * 
     * @param emptyFuse
     */
    public void fuseRight(TFNode emptyFuse){
        //Naming the node our empty node, emptyFuse, is fusing with,
        //and their parent
        TFNode parent = emptyFuse.getParent();
        TFNode rightSibling = emptyFuse.getParent().getChild(1);
        
        //Slide leftmost Item from parent down to node to fuse with.
        
        rightSibling.insertItem(0, parent.removeItem(0));
        if(parent == root() && root().getNumItems() == 0){
            setRoot(rightSibling);
            rightSibling.setParent(null);
            
        }
        //Move child over
        if( !(rightSibling.isExternal()) ){
            TFNode fuseChild = emptyFuse.getChild(0);
            rightSibling.setChild(0, fuseChild);
            rightSibling.getChild(0).setParent(rightSibling);
            if(rightSibling.getNumItems()==4){
                fixInsert(rightSibling);
            }
        }
        
        //Connect rightSibling's parent to rightSibling again (needed?)
        //parent.setChild(0, rightSibling);
        
        //If fuse emptied parent, call fixUnderflow on it
        if(rightSibling != root()){
            if(parent.getNumItems() == 0){
                fixUnderflow(parent);
                size--;
            }
        }
        size--;
    }    
    
    public static void main(String[] args) {
        
        Comparator myComp = new IntegerComparator();
        TwoFourTree myTree = new TwoFourTree(myComp);
        final int TEST_SIZE = 10000;
        
        //The random test case that no longer breaks our tree!
        Integer[] testArray = new Integer[TEST_SIZE];
        Random rand = new Random(2);
        for(int i = 0; i < TEST_SIZE; i++){
            testArray[i] = rand.nextInt(TEST_SIZE/5);
            System.out.println("    inserting "+testArray[i]);
            myTree.insertElement(testArray[i], testArray[i]);
        }
        System.out.println(TEST_SIZE+" items inserted to tree successfully");
        myTree.printAllElements();
        System.out.println("removing");
        for(int i = 0; i < TEST_SIZE; i++){
            
            if (i > TEST_SIZE - 15){
                System.out.println("removing " + testArray[i]);
            }
            int out = (Integer) myTree.removeElement(testArray[i]);
            if (out != testArray[i]) {
                throw new TwoFourTreeException("main: wrong element removed");
            }
            if (i > TEST_SIZE - 15){
                myTree.printAllElements();
            }
            myTree.checkTree();
        }
        System.out.println(myTree.size);
    }

    public void printAllElements() {
        int indent = 0;
        if (root() == null) {
            System.out.println("The tree is empty");
        }
        else {
            printTree(root(), indent);
        }
    }

    public void printTree(TFNode start, int indent) {
        if (start == null) {
            return;
        }
        for (int i = 0; i < indent; i++) {
            System.out.print(" ");
        }
        printTFNode(start);
        indent += 4;
        int numChildren = start.getNumItems() + 1;
        for (int i = 0; i < numChildren; i++) {
            printTree(start.getChild(i), indent);
        }
    }

    public void printTFNode(TFNode node) {
        int numItems = node.getNumItems();
        for (int i = 0; i < numItems; i++) {
            System.out.print(((Item) node.getItem(i)).element() + " ");
        }
        System.out.println();
    }

    // checks if tree is properly hooked up, i.e., children point to parents
    public void checkTree() {
        checkTreeFromNode(treeRoot);
    }

    private void checkTreeFromNode(TFNode start) {
        if (start == null) {
            return;
        }

        if (start.getParent() != null) {
            TFNode parent = start.getParent();
            int childIndex;
            for(childIndex = 0; childIndex <= parent.getNumItems(); 
                    childIndex++) {
                if (parent.getChild(childIndex) == start) {
                    break;
                }
            }
            // if child wasn't found, print problem
            if (childIndex > parent.getNumItems()) {
                System.out.println("Child to parent confusion");
                printTFNode(start);
            }
        }

        if (start.getChild(0) != null) {
            for (int childIndex = 0; childIndex <= start.getNumItems(); 
                    childIndex++) {
                if (start.getChild(childIndex) == null) {
                    System.out.println("Mixed null and non-null children");
                    printTFNode(start);
                }
                else {
                    if (start.getChild(childIndex).getParent() != start) {
                        System.out.println("Parent to child confusion");
                        printTFNode(start);
                    }
                    for (int i = childIndex - 1; i >= 0; i--) {
                        if (start.getChild(i) == start.getChild(childIndex)) {
                            System.out.println("Duplicate children of node");
                            printTFNode(start);
                        }
                    }
                }

            }
        }

        int numChildren = start.getNumItems() + 1;
        for (int childIndex = 0; childIndex < numChildren; childIndex++) {
            checkTreeFromNode(start.getChild(childIndex));
        }

    }
}
